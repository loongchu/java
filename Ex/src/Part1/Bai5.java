package Part1;
import java.util.Scanner;
public class Bai5 {
	public static void main(String[] args) {
		int n,x ;
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		int sum = 0;
		while(n > 0) {
			x = n % 10;
			n /= 10;
			sum += x;
		}
		System.out.println(sum);
	}
}
