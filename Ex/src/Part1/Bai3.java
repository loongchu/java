package Part1;

import java.util.Scanner;

public class Bai3 {
	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap so n :");
		n = sc.nextInt();
		System.out.printf("%d so nguyen to dau tien la \n", n);
		int count = 0;
		int i = 2;
		while (count < n) {
			if (isNguyenTo(i)) {
				System.out.println(i + " ");
				count++;
			}
			i++;
		}

	}

	public static boolean isNguyenTo(int x) {
		if (x < 2) {
			return false;
		}

		for (int i = 2; i <= Math.sqrt(x); i++) {
			if (x % i == 0) {
				return false;
			}
		}
		return true;

	}
}
