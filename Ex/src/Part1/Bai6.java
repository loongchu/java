package Part1;

import java.util.Scanner;

public class Bai6 {
	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		int f0 = 0;
		int f1 = 1;
		int fn = 0;
		for (int i = 0; i <= n; i++) {
			if (i < 2) {
				System.out.println(i);

			} else {
				fn = f0 + f1;
				f0 = f1;
				f1 = fn;
				System.out.println(fn);

			}

		}

	}
}
