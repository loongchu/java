package Arrays;

import java.util.Arrays;

public class Bai1 {
			// Mảng là 1 tập hợp các biến cùng kiểu
			// Kích thước của mảng không thể thay đổi 
			// Các phần tử đó được lưu trữ trong các ô cạnh nhau với các chỉ số 
			public static void main(String[] args) {
				int[] myArray = new int[5];
				Arrays.fill(myArray, 2);
				int sum = 0;
				for(int i = 0; i<myArray.length ; i++) {
					if(i == 0 || i%2 == 0) {
						myArray[i] = myArray[i] * 1;
						System.out.println(myArray[i]);
					}
					else {
						myArray[i] = myArray[i] * (-1);
						System.out.println(myArray[i]);
					}
					sum +=  myArray[i];
				}
				System.out.println(sum);
				
			}
}
