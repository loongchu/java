package Arrays;

import java.util.Scanner;
// Kiểm tra dãy có số nguyên tố hya k ? Có phải tất cả các phần tử của dãy đều là số nguyên tố ?
public class Bai4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a[] = new int[n];
		for (int i = 0; i < a.length; i++) {
			a[i] = sc.nextInt();
		}
		System.out.print("Trong day co so nguyen to la : ");
		// Trỏ đến check
		for (int i = 0; i < a.length; i++) {
			
			if (check(a[i]) == true) {
				System.out.print(a[i] + ",");
			}
			
		}
			System.out.println( );
			System.out.print("Day co phai day nguyen to hay k : ");
			// Trỏ đến checkAll
			if (checkAll(a, n) == true) {
				System.out.println("True.");
			}
			else {
				System.out.println("False.");
			}
		
	}
	// check số có phải nguyên tố hay k ?
	public static boolean check(int n) {
		if (n < 2)
			return false;
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}

		return true;
	}
	// check xem có phải tất cả phần tử trong dãy có phải số nguyên tố hay k ?
	public static boolean checkAll(int a[], int k) {
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			if (check(a[i]) == true) {
				count++;
				if(count == k) {
					return true;
				}
			} 
		}

		return false;

	}
}
