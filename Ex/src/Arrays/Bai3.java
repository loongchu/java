package Arrays;

import java.util.Scanner;
// Bài này là : Nhập số thực a và số k . Xét xem trong dãy có k số dương đứng cạnh nhau hay k ?
public class Bai3 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a[] = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		System.out.println("nhap so k");
		 int k = sc.nextInt();
		if(kt(a,k)==true) {
			System.out.print("true");
		}else {
			System.out.print("false");
		}
	}
	// Check xem có bao nhiêu số dương đứng cạnh nhau
	public static boolean kt(int a[], int n) {
		int t = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] > 0) {
				t++;
			} else {
				if (t == n) {
					return true;
				} else {
					t = 0;
				}
			}
		}
		return false;

	}

	
}
