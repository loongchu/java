package Chapter3;

public class NamePlayer {
	public static void main(String[] args) {
		 
        // Khai báo một mảng 2 chiều, chỉ rõ các phần tử của mảng.
        String[][] teamAndPlayers = new String[][] { 
                { "Sam", "Smith", "Robert" }, // US Players
                { "Tran", "Nguyen" }          // Vietnam Players
        };
 
        String[] usPlayers = teamAndPlayers[0];
        String[] vnPlayers = teamAndPlayers[1];
         
        System.out.println("Team count: " + teamAndPlayers.length); // ==> 2
 
        System.out.println("Us Players count:" + usPlayers.length); // ==> 3
        System.out.println("Vn Players count:" + vnPlayers.length); // ==> 2
 
        for (int row = 0; row < teamAndPlayers.length; row++) { 
 
            String[] players = teamAndPlayers[row];
 
            for (int col = 0; col < players.length; col++) {
                System.out.println("Player at[" + row + "][" + col + "]=" + teamAndPlayers[row][col]);
            }
 
        }
 
    }
}
