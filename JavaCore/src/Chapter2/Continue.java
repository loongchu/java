package Chapter2;

public class Continue {
	public static void main(String[] args) {
		FIRST_CHAR_LOOP: for (int a = 1; a <= 4; a++) {
			for (char x = 'a'; x <= 'c'; x++) {
				if (a == 2 || x == 'b')
					// Các trường hợp a = 2 hoặc x = 'b' sẽ không bao h dc in ra 
					// Vì nó sẽ gặp lệnh continue rồi trỏ đến vòng lặp ngoài
					continue FIRST_CHAR_LOOP;
				System.out.print(" " + a + x);
			}
		}
	}
}
