package Chapter2;

public class Switch {
		public static void main(String[] args) {
			//Khai bao 1 bien age (20)
			int age = 20;
			switch(age) {
			// Truong hop tuoi bang 18
			case 18:
				System.out.println("You are 18 years old");
				break;
			// Truong hop tuoi bang 20	
			case 20:
				System.out.println("You are 20 years old");
				break;
			// Cac truong hop con lai
			default:
				System.out.println("You are not 18 or 20 years old");
			}
			// Thu 1 truong hop khac
			int dayOfWeek = 5;
			switch(dayOfWeek) {
			 case 0:
				System.out.println("Sunday");
			 // Lan nay se k co break
			 default:
				System.out.println("Weekday");
			// k co break tiep 
			 case 6:
				System.out.println("Saturday");
			 break;
			}			
		}
}
