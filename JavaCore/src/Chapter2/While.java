package Chapter2;

public class While {
	public static void main(String[] args) {
		eatCheese(10);
	}
	public static void eatCheese(int biteOfCheese) {
		int roomInBelly = 5 ;
		while(biteOfCheese > 0 && roomInBelly > 0) {
			biteOfCheese--;
			roomInBelly--;
		}
		System.out.println(biteOfCheese + "+" + roomInBelly);
		}
	}

