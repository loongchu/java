package Chapter2;

public class For {
	public static void main(String[] args ) {
		// Khai bao 1 bien step , mo ta buoc cua vong lap 
		int step = 1;
		//Khai bao 1 bien value voi gia trij bat dau bang 0
		//Sau moi mot buoc lap value lai duoc cong them 3 
		//Va vong lap se ket thuc khi value lon hon hoac bang 10
		for(int value = 0; value < 10 ; value +=3) {
			System.out.println("Step "+ step + "  "+ "value " + value);
			step++;
		}
		
	}
}
