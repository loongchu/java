package Chapter2;

public class DoWhile {
	public static void main(String[] args) {
		int value = 3;
		// Vong lap do-while luon duoc thuc hien it nhat 1 lan
		do {
			System.out.println("Value = "+ value);
			//Tang gia tri cho value them 2
			value += 2;
		}while(value < 10);
		
	}
}
